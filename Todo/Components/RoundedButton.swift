//
//  RoundedButton.swift
//  Todo
//
//  Created by CJ Good on 5/4/22.
//

import SwiftUI

struct RoundedButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(.orange)
            .foregroundColor(Color("#293241"))
            .cornerRadius(10)
            .scaleEffect(configuration.isPressed ? 0.95 : 1)
    }
}

struct CustomLabel: LabelStyle {
    func makeBody(configuration: Configuration) -> some View {
        Label(configuration)
            .padding()
            .foregroundColor(Color(.orange))
            .background(Color("#293241"))
    }
}
