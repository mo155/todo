//
//  TodoItem.swift
//  Todo
//
//  Created by CJ Good on 5/3/22.
//

import SwiftUI

struct TodoItem: View {
    @Environment(\.managedObjectContext) private var itemContext
    
    @ObservedObject var item: Item
    
    @State var showErrorMessage: Bool = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 1) {
            Text(item.title ?? "")
                .fontWeight(.bold)
                .strikethrough(item.completed)
            Text(item.subtitle ?? "")
                .italic()
                .strikethrough(item.completed)
        }
        .onReceive(self.item.objectWillChange) {
            do {
                try self.itemContext.save()
            } catch {
                print("Error")
            }
        }.alert(isPresented: $showErrorMessage) {
            Alert(
                title: Text("Error"),
                message: Text("Something went wrong!"),
                dismissButton: .default(Text("Dismiss")))
        }
    }
}
