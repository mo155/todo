//
//  AddTodoPopup.swift
//  Todo
//
//  Created by CJ Good on 5/3/22.
//

import SwiftUI
import CoreData

struct AddTodoPopup: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @Binding var showAddModal: Bool
    @State private var title: String = ""
    @State private var description: String = ""
    
    var body: some View {
        VStack(alignment: .center, spacing: 2) {
            Spacer()
            
            VStack(alignment: .leading, spacing: 1) {
                Text("Add New Todo")
                    .bold()
                    .font(Font.title2)
                    .padding([.bottom], 5)
                    .foregroundColor(Color(.orange))
                TextField("Enter a new title...", text: $title)
                    .textFieldStyle(.roundedBorder)
            }.padding([.bottom], 10)
            
            VStack(alignment: .leading, spacing: 1) {
                Text("Describe the Task")
                    .bold()
                    .font(Font.title2)
                    .padding([.bottom], 5)
                    .foregroundColor(Color(.orange))
                TextField("Describe the task here...", text: $description)
                    .textFieldStyle(.roundedBorder)
            }.padding([.bottom], 10)
            
            Button {
                addItem(title: title, subtitle: description)
            } label: {
                Label("Add Item", systemImage: "plus")
                    
            }.buttonStyle(RoundedButtonStyle())
                        
            Spacer()
            
            Button {
                self.showAddModal.toggle()
            } label: {
                Text("Dismiss")
            }.foregroundColor(Color(.orange))
                
        }
        .padding([.leading, .trailing], 20)
        .background(Color("#293241")?.ignoresSafeArea())
    }
    
    private func addItem(title: String, subtitle: String) {
        withAnimation {
            let newItem = Item(context: viewContext)
            newItem.createdAt = Date()
            newItem.updatedAt = Date()
            newItem.title = title
            newItem.subtitle = subtitle
            
            do {
                try viewContext.save()
                self.showAddModal.toggle()
            } catch {
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}
