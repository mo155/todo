//
//  Theme.swift
//  Todo
//
//  Created by CJ Good on 5/4/22.
//

import Foundation
import SwiftUI

struct Theme {
    public var palette: Palette
    init() {
        self.palette = Palette(
            primary: Color(red: 41.0, green: 50.0, blue: 65.0),
            secondary: Color(red: 238.0, green: 108.0, blue: 77.0)
        )
    }
}

struct Palette {
    public var primary: Color
    public var secondary: Color
    init(primary: Color, secondary: Color) {
        self.primary = primary
        self.secondary = secondary
    }
}
