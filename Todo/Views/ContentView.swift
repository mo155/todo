//
//  ContentView.swift
//  Todo
//
//  Created by CJ Good on 5/3/22.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @Environment(\.colorScheme) var colorScheme
    @Environment(\.managedObjectContext) private var viewContext
    
    @State private var showAddModal = false
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Item.createdAt, ascending: true)],
        animation: .default)
    private var items: FetchedResults<Item>
    
    // Set the background for the List View Sidebar
    init(){
        UITableView
            .appearance()
            .backgroundColor = UIColor.init(Color("#e76f51"))
    }
    
    var body: some View {
        VStack {
            HStack {
                HStack(alignment: VerticalAlignment.center, spacing: 10) {
                    Text("Todos")
                        .bold()
                        .font(Font.title)
                        .foregroundColor(.orange)
                    Spacer()
                    Button {
                        self.showAddModal.toggle()
                    } label: {
                        Image(systemName: "square.and.pencil")
                    }.tint(Color(.orange))
                }
                .padding([.leading, .trailing], 30)
                .padding([.bottom], 10)
                .sheet(isPresented: $showAddModal) {
                    AddTodoPopup(showAddModal: self.$showAddModal)
                }
            }
            .background(Color("#293241")
            .ignoresSafeArea())
            
            List {
                ForEach(items, id: \.self) { item in
                    TodoItem(item: item)
                    .swipeActions(edge: .leading, allowsFullSwipe: true) {
                        Button {
                            item.completed.toggle()
                        } label: {
                            item.completed
                                ? Label("Mark Incomplete", systemImage: "arrow.uturn.backward")
                                : Label("Mark Complete", systemImage: "checkmark")
                        }.tint(.orange)
                    }
                    .swipeActions(edge: .trailing, allowsFullSwipe: true) {
                        Button(role: .destructive) {
                            viewContext.delete(item)
                        } label: {
                            Label("Delete", systemImage: "trash")
                        }
                    }
                }
                .listRowSeparator(.automatic)
                .foregroundColor(colorScheme == .dark ? Color(.orange) : Color("#293241"))
            }
            .listStyle(.plain)
            .padding([.leading, .trailing], 0)
        }
        .listStyle(.plain)
        .padding([.leading, .trailing], 0)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView().previewDevice("iPhone 13 Pro").environment(\.managedObjectContext, PersistenceController.preview.container.viewContext).previewInterfaceOrientation(.portrait)
            ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext).previewInterfaceOrientation(.landscapeLeft)
        }
    }
}

extension Color {
    init?(_ hex: String) {
        var str = hex
        if str.hasPrefix("#") {
            str.removeFirst()
        }
        if str.count == 3 {
            str = String(repeating: str[str.startIndex], count: 2)
            + String(repeating: str[str.index(str.startIndex, offsetBy: 1)], count: 2)
            + String(repeating: str[str.index(str.startIndex, offsetBy: 2)], count: 2)
        } else if !str.count.isMultiple(of: 2) || str.count > 8 {
            return nil
        }
        guard let color = UInt64(str, radix: 16)
        else {
            return nil
        }
        if str.count == 2 {
            let gray = Double(Int(color) & 0xFF) / 255
            self.init(.sRGB, red: gray, green: gray, blue: gray, opacity: 1)
        } else if str.count == 4 {
            let gray = Double(Int(color >> 8) & 0x00FF) / 255
            let alpha = Double(Int(color) & 0x00FF) / 255
            self.init(.sRGB, red: gray, green: gray, blue: gray, opacity: alpha)
        } else if str.count == 6 {
            let red = Double(Int(color >> 16) & 0x0000FF) / 255
            let green = Double(Int(color >> 8) & 0x0000FF) / 255
            let blue = Double(Int(color) & 0x0000FF) / 255
            self.init(.sRGB, red: red, green: green, blue: blue, opacity: 1)
        } else if str.count == 8 {
            let red = Double(Int(color >> 24) & 0x000000FF) / 255
            let green = Double(Int(color >> 16) & 0x000000FF) / 255
            let blue = Double(Int(color >> 8) & 0x000000FF) / 255
            let alpha = Double(Int(color) & 0x000000FF) / 255
            self.init(.sRGB, red: red, green: green, blue: blue, opacity: alpha)
        } else {
            return nil
        }
    }
}
