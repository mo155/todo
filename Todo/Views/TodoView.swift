//
//  TodoView.swift
//  Todo
//
//  Created by CJ Good on 5/3/22.
//

import SwiftUI

struct TodoView: View {
    var item: Item
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .top) {
                Text(item.title!).fontWeight(.bold)
            }
            Spacer()
        }
    }
}

